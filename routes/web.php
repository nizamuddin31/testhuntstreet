<?php

use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/invitation/{token}', [App\Http\Controllers\InvitationController::class, 'formInvitation'])->name('form-invitation');
Route::post('/invitation/{token}', [App\Http\Controllers\InvitationController::class, 'addDataInvitation'])->name('add-data-invitation');
// Route::post('/login', [App\Http\Controllers\AuthController::class, 'login'])->name('login');
// Route::get('/login', [App\Http\Controllers\AuthController::class, 'loginPage'])->name('loginPage');

Route::middleware(['auth'])->group(function() {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('index-home');

    Route::middleware(['role.verify'])->group(function() {
        Route::group(['prefix' => 'customer'], function() {
            Route::get('/', [App\Http\Controllers\CustomerController::class, 'index'])->name('index-customer');
            Route::get('/add', [App\Http\Controllers\CustomerController::class, 'addPage'])->name('add-customer');
            Route::post('/add', [App\Http\Controllers\CustomerController::class, 'addData'])->name('add-data-customer');
            Route::get('/edit/{id_cusomter}', [App\Http\Controllers\CustomerController::class, 'editPage'])->name('edit-customer');
            Route::post('/edit/{id_cusomter}', [App\Http\Controllers\CustomerController::class, 'editData'])->name('edit-data-customer');
            Route::get('/delete/{id_cusomter}', [App\Http\Controllers\CustomerController::class, 'deleteData'])->name('delete-customer');
            Route::get('/invitation', [App\Http\Controllers\CustomerController::class, 'invitationPage'])->name('invitaion-form-customer');
            Route::post('/sent-invitation', [App\Http\Controllers\CustomerController::class, 'sentLinkInvitation'])->name('sent-invitation-link-customer');
        });
    });
});

