<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersInvitationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers_invitation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_customer');
            $table->string('name');
            $table->timestamp('birth_date')->nullable();
            $table->string('gender', 2);
            $table->unsignedBigInteger('id_designer');
            $table->unsignedBigInteger('id_status_invitation');
            $table->timestamps();
            $table->foreign('id_customer')->references('id')->on('customers');
            $table->foreign('id_designer')->references('id')->on('designer');
            $table->foreign('id_status_invitation')->references('id')->on('status_invitation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_invitation');
    }
}
