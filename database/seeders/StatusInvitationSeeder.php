<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class StatusInvitationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_invitation')->insert([
            [
                'key'=> 'just_entered',
                'name'=> 'Baru Di Masukan'
            ],
            [
                'key'=> 'email_has_been_sent',
                'name'=> 'Email Ucapan Telah Terkirim'
            ],
        ]);
    }
}
