<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class DesignerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designer')->insert([
            [
                'name'=> 'A Bathing Ape',
            ],
            [
                'name'=> 'A.W.A.K.E',
            ],
            [
                'name'=> 'Carolina Herrera',
            ],
            [
                'name'=> 'Dolce & Gabbana',
            ],
            [
                'name'=> 'Fred Perry Sportswear',
            ],
            [
                'name'=> 'Hender Scheme',
            ],
            [
                'name'=> 'Les Merveilleuses LADURÉE',
            ],
        ]);
    }
}
