<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setting')->insert([
            [
                'key'=> 'date_event',
                'value' => '2021-12-01 12:00:00',
            ],
        ]);
    }
}
