<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert([
            [
                'name'=> 'admin',
                'id_user' => 1,
                'tlpn' => "0834543599",
                'address' => "Alamat",
                'is_delete'  => 0,
            ],
        ]);
    }
}
