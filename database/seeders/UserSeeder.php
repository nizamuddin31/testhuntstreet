<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use DB;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'=> 'admin',
                'email' => 'Admin@admin.com',
                'password' => Hash::make('password'),
                'active' => 1,
                'role'  => 'admin',
            ],
            [
                'name'=> 'customer',
                'email' => 'customer@customer.com',
                'password' => Hash::make('password'),
                'active' => 1,
                'role'  => 'customer',
            ],
        ]);
    }
}
