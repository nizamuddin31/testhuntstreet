<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name'=> 'custmoer',
                'id_user' => 1,
                'tlpn' => "0834543599",
                'address' => "Alamat",
                'is_delete'  => 0,
            ],
        ]);
    }
}
