<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use App\Models\CustomerModel;
use App\Models\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->customers = CustomerModel::where('is_delete', 0)->get();
        return $this->view('customer.index', ['customers']);
    }

    public function addPage(Request $request)
    {
        $this->customer = false;
        return $this->view('customer.form', ['customer']);
    }

    public function addData(Request $request)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'tlpn' => 'required',
                'address' => 'required',
                'email' => 'required|unique:users|email',
            ]);

            if ($validator->fails()){
                throw new \Exception($validator->errors()->first());
            }

            $userModel = new UsersModel;
            $userModel->name = $request->name;
            $userModel->email = $request->email;
            $userModel->password = Hash::make('password');
            $userModel->role = 'customer';
            $userModel->active = 1;

            if(!$userModel->save()){
                throw new \Exception("Data Gagal Disimpan");
            }

            $customerModel = new CustomerModel;
            $customerModel->name = $request->name;
            $customerModel->tlpn = $request->tlpn;
            $customerModel->address = $request->address;
            $customerModel->id_user = $userModel->id;

            if(!$customerModel->save()){
                throw new \Exception("Data Gagal Disimpan");
            }

            DB::commit();

            $res = [
                'url' => route('index-customer')
            ];

            return $this->success_response("Berhasil Menambahkan Data Pelanggan", $res, $request->all());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->failed_response($e->getMessage());
        }
    }

    public function editPage(Request $request, $id_customer)
    {
        if (! DB::table('customers')->where('id', $id_customer)->first()) {
            abort(404);
        }

        $this->customer = CustomerModel::where('id', $id_customer)->where('is_delete', 0)->first();
        return $this->view('customer.form', ['customer']);
    }

    public function editData(Request $request, $id_customer)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'tlpn' => 'required',
                'address' => 'required',
                'email' => 'required|email',
            ]);

            if ($validator->fails()){
                throw new \Exception($validator->errors()->first());
            }

            $customerModel = CustomerModel::find($id_customer);
            $userModel = UsersModel::find($customerModel->id_user);

            $userModel->name = $request->name;
            $userModel->email = $request->email;

            if(!$userModel->save()){
                throw new \Exception("Data Gagal Disimpan");
            }

            $customerModel->name = $request->name;
            $customerModel->tlpn = $request->tlpn;
            $customerModel->address = $request->address;
            $customerModel->id_user = $userModel->id;

            if(!$customerModel->save()){
                throw new \Exception("Data Gagal Disimpan");
            }

            DB::commit();

            $res = [
                'url' => route('index-customer')
            ];

            return $this->success_response("Berhasil Menambahkan Data Pelanggan", $res, $request->all());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->failed_response($e->getMessage());
        }
    }

    public function deleteData($id_customer)
    {
        try {
            if (! DB::table('customers')->where('id', $id_customer)->first()) {
                abort(404);
            }

            $customerModel = CustomerModel::find($id_customer);
            $userModel = UsersModel::find($customerModel->id_user);

            $customerModel->is_delete = 1;
            if(!$customerModel->save()){
                throw new \Exception("Data Gagal Disimpan");
            }

            $userModel->active = 0;
            if(!$userModel->save()){
                throw new \Exception("Data Gagal Disimpan");
            }
            return redirect()->route('index-customer')->with(['success' => "Data Berhasil Tersimpan !"]);
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with(['error' => $e->getMessage()]);
        }
    }

    public function invitationPage(Request $request)
    {
        $this->customers = CustomerModel::where('is_delete', 0)->whereNull('invitation_token')->get();
        return $this->view('customer.invitation-form', ['customers']);
    }

    public function sentLinkInvitation(Request $request)
    {
        DB::beginTransaction();
        try {
            if(!$request->is_all_customer){
                if(!$request->id_customer){
                    throw new \Exception("Silahkan Pilih Data Karyawan");
                }
            }

            $eloquent = CustomerModel::select('customers.*', 'users.email')->join('users', 'users.id', '=', 'id_user')->where('is_delete', 0)->whereNull('invitation_token');
            if($request->id_customer){
                $eloquent = $eloquent->whereIn('customers.id',$request->id_customer);
            }
            $customers = $eloquent->get();
            foreach ($customers as $key => $customer) {
                $send_mail = $customer->email;
                $token = base64_encode($send_mail.'=>'.$customer->id);
                dispatch(new SendEmailJob($send_mail,'email_link', $token));
                $customer->invitation_token = $token;
                if(!$customer->save()){
                    throw new \Exception("Data Gagal Dikirim");
                }
            }
            DB::commit();
            $res = [
                'url' => route('index-customer')
            ];

            return $this->success_response("Berhasil Menambahkan Data Pelanggan", $res, $request->all());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->failed_response($e->getMessage());
        }
    }
}
