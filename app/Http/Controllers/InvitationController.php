<?php

namespace App\Http\Controllers;

use App\Models\CustomerModel;
use App\Models\CustomersInvitationModel;
use App\Models\DesignerModel;
use App\Models\SettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class InvitationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function formInvitation(Request $request)
     {
        DB::beginTransaction();
        try {
            if(!$request->token){
                throw new \Exception("Invalid Token");
            }

            $token = $request->token;
            $this->customer = CustomerModel::where('invitation_token', $token)->first();
            if(!$this->customer){
                throw new \Exception("Invalid Token");
            }

            $settingModel = SettingModel::where('key', 'date_event')->first();
            $this->date_event = $settingModel->value;
            $today = date("Y-m-d H:i:s");

            if ($this->date_event < $today) {
                // dd("asd");
                abort(403, 'Unauthorized action.');
            }

            $decode_token = base64_decode($token);
            $explode_token = explode("=>", $decode_token);
            $id_customer = $explode_token[1];

            if(!$this->customer->invitation){
                $this->designers = DesignerModel::all();
                $view = 'invitation.form';
                $data = ['customer', 'designers', 'date_event'];
            }else{
                $customer_invitation_model = CustomersInvitationModel::where('id_customer', $id_customer)->first();
                $this->regist_code = $customer_invitation_model->code;
                $view = 'invitation.random-code';
                $data = ['customer',  'date_event', 'regist_code'];
            }
            DB::commit();
            return $this->view($view, $data);
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with(['error' => $e->getMessage()]);
        }
     }

     public function addDataInvitation(Request $request)
     {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'gender' => 'required',
                'birth_date' => 'required',
                'deigner' => 'required',
            ]);

            if ($validator->fails()){
                throw new \Exception($validator->errors()->first());
            }

            $token = $request->token;
            $this->customer = CustomerModel::where('invitation_token', $token)->first();
            if(!$this->customer){
                throw new \Exception("Invalid Token");
            }

            $decode_token = base64_decode($token);
            $explode_token = explode("=>", $decode_token);
            $id_customer = $explode_token[1];
            
            $customer_invitation_model = new CustomersInvitationModel;
            $customer_invitation_model->code = $customer_invitation_model->createRandomCode();
            $customer_invitation_model->id_customer = $id_customer;
            $customer_invitation_model->name = $request->name;
            $customer_invitation_model->birth_date = date('Y-m-d H:i:d', strtotime($request->birth_date));
            $customer_invitation_model->gender = $request->gender;
            $customer_invitation_model->id_designer = $request->deigner;
            $customer_invitation_model->id_status_invitation = 1;

            if(!$customer_invitation_model->save()){
                throw new \Exception("Data Gagal Tersimpan");
            }

            $res = [
                'url' => route('form-invitation', ['token'=> $request->token])
            ];
            DB::commit();
            return $this->success_response("Berhasil Menambahkan Data Pelanggan", $res, $request->all());

        } catch (\Exception $e) {
            DB::rollback();
            return $this->failed_response($e->getMessage());
        }
     }
}
