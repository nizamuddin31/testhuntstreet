<?php

namespace App\Http\Middleware;

use App\Models\MenuRoleModel;
use App\Models\RoleModel;
use Closure;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth ;
use Illuminate\Support\Facades\URL;

class RoleValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if(Auth::user()->role != 'admin'){
                throw new Exception("Tidak Terdapat Akses");
            }
        } catch (Exception $e) {
            return redirect()->route('index-home')->with(['error' => $e->getMessage()]);;
        }
        return $next($request);
    }
}