<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\SendEmailDemo;
use App\Mail\ThankyouNoteEmail;
use Mail;
use Illuminate\Queue\SerializesModels;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $send_mail;
    protected $token;
    protected $type_email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($send_mail, $type_email, $token = '')
    {   
        $this->send_mail = $send_mail;
        $this->token = $token;
        $this->type_email = $type_email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->type_email == 'email_link'){
            $email = new SendEmailDemo($this->token);        
            Mail::to($this->send_mail)->send($email);
        }else{
            $email = new ThankyouNoteEmail();        
            Mail::to($this->send_mail)->send($email);
        }
    }
}
