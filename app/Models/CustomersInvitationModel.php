<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomersInvitationModel extends Model
{
    use HasFactory;

    protected $table = 'customers_invitation';

    function createRandomCode() { 

        $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
        srand((double)microtime()*1000000); 
        $i = 0; 
        $code = '' ; 
    
        while ($i <= 7) { 
            $num = rand() % 33; 
            $tmp = substr($chars, $num, 1); 
            $code = $code . $tmp; 
            $i++; 
        } 
    
        return strtoupper($code); 
    
    } 

    public function status_invitation()
    {
        return $this->belongsTo(StatusInvitationModel::class, 'id_status_invitation');
    }

    public function customer()
    {
        return $this->belongsTo(CustomerModel::class, 'id_customer');
    }
}
