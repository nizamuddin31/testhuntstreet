<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    // use HasFactory;
    protected $table = 'customers';

    public function users(){
        return $this->belongsTo(UsersModel::class, 'id_user');
    }

    public function invitation()
    {
        return $this->hasOne(CustomersInvitationModel::class, 'id_customer');
    }

    public function invitation_status()
    {
        $invitation = $this->invitation;
        if(!$this->invitation_token){
            return '<label class="label label-warning">Link Undangan Belum Terkirim</label>';
        }else{
            if(!$invitation){
                return '<label class="label label-info">Link Undangan Telah Terkirim, Form Belum Terisi</label>';
            }else{
                return '<label class="label label-info">'.$invitation->status_invitation->name.'</label>';
            }
        }
    }
}
