<?php

namespace App\Console\Commands;

use App\Jobs\SendEmailJob;
use App\Models\CustomersInvitationModel;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ThankyouNoteEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:thankyouNoteEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            $customers_invitation = CustomersInvitationModel::where('created_at', '<', 
                    Carbon::now()->subHours(1)->toDateTimeString()
                )
                ->where('id_status_invitation', 1)->get();
            foreach ($customers_invitation as $key => $customer_invitation) {
                $send_mail=$customer_invitation->customer->users->email;
                dispatch(new SendEmailJob($send_mail,'email_thankyou_note'));
                $customer_invitation->id_status_invitation = 2;
                if(!$customer_invitation->save()){
                    throw new \Exception("Gagal");
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            // return $this->failed_response($e->getMessage());
        }
    }
}