function sweet_alert(icon, title , text, showCancelButton =  false, cancelButtonText ='Tidak', confirmButtonText = 'OK', html = ''){
    return Swal.fire({
        title: title,
        icon: icon,
        html: text,
        reverseButtons: !0,
        showCancelButton : showCancelButton,
        cancelButtonText : cancelButtonText,
        confirmButtonText : confirmButtonText,
        allowOutsideClick: false,
    })
}

function loader(){
    return Swal.fire({
          title: 'Mohon Tunggu',
          width: 600,
          padding: '3em',
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          }
        });
}

// $(document).ready(function(){
$(document).on('submit', '#form', function () {
    var formData = new FormData(this);
    // console.log(formData);return false
    $.ajax({
        url:$(this).attr("action"),
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        type:$(this).attr("method"),
        dataType: 'html',
        beforeSend: function() {
            // loader()
        },
        complete:function() {
            
        },
        success:function(data) {
            let res = JSON.parse(data)
            if(res.success == false){
                sweet_alert("error", "Error", res.message).then(function (e) {
                    e.dismiss;
                }, function (dismiss) {
                    return false;
                })
            }else{
                console.log(res);
                sweet_alert("success", "Berhasil", res.message).then(function (e) {
                    window.location.href = res.data.url;
                }, function (dismiss) {
                    return false;
                })
            }
        }
    })
    return false
})
// })