@extends('layout.layout-login')

@section('content')

<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
    <!-- Container-fluid starts -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <!-- Authentication card start -->
                <div class="login-card card-block auth-body mr-auto ml-auto">
                    <form class="md-float-material" method="POST" action="{{ route('add-data-invitation', ['token' => $customer->invitation_token]) }}" enctype="multipart/form-data"  id="form">
                        @csrf
                        <div class="text-center">
                            <img src="{{asset('assets/')}}/images/auth/logo-dark.png" alt="logo.png">
                        </div>
                        <div class="auth-box">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-left txt-primary">Form Undangan</h3>
                                </div>
                            </div>
                            <hr/>
                            <span class="text-left text-dark"><b>Nama Pelanggan</b></span>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Nama Pelanggan" name="name" value="{{ $customer ? $customer->name : '' }}">
                                <span class="md-line"></span>
                            </div>

                            <span class="text-left text-dark"><b>Email</b></span>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Email Pelanggan" name="email" value="{{ $customer ? $customer->users->email : '' }}" readonly>
                                <span class="md-line"></span>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <span class="text-left text-dark"><b>Jenis Kelamin</b></span>
                                    <div class="input-group">
                                        <select name="gender" class="form-control">
                                            <option value="">Jenis Kelamin</option>
                                            <option value="L">Laki Laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                        <span class="md-line"></span>
                                    </div>  
                                </div>
                                <div class="col-md-6">
                                    <span class="text-left text-dark"><b>Tanggal Lahir</b></span>
                                    <div class="input-group">
                                        <input type="date" class="form-control" placeholder="Tanggal Lahir" name="birth_date" >
                                        <span class="md-line"></span>
                                    </div>
                                </div>
                            </div>
                            <span class="text-left text-dark"><b>Designer</b></span>
                            <div class="input-group">
                                <select name="deigner" class="form-control">
                                    <option value="">Desinger</option>
                                    
                                    @foreach($designers as $designer)
                                    <option value="{{ $designer->id }}">{{ $designer->name }}</option>
                                    @endforeach
                                </select>
                                <span class="md-line"></span>
                            </div>

                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Submit</button>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-md-10">
                                    <p class="text-inverse text-left m-b-0">Batas Pengumpulan Form</p>
                                    <p class="text-inverse text-left"><b id="countdown"></b></p>
                                </div>
                                <div class="col-md-2">
                                    <img src="{{asset('assets/')}}/images/auth/Logo-small-bottom.png" alt="small-logo.png">
                                </div>
                            </div>

                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- Authentication card end -->
            </div>
            <!-- end of col-sm-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of container-fluid -->
</section>

@endsection

@section('javascript')
<script>
    // Set the date we're counting down to
    var countDownDate = new Date("{{ $date_event }}").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();
        
    // Find the distance between now and the count down date
    var distance = countDownDate - now;
        
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
    // Output the result in an element with id="demo"
    document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
        
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }
    }, 1000);
</script>
@endsection