@extends('layout.layout-main')
@section('content')

<!-- Page-header start -->
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont icofont icofont-file-document bg-c-pink"></i>
                <div class="d-inline">
                    <h4>Sample Page</h4>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index.html">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Pages</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Sample page</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Daftar Pelanggan</h5>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option" style="width: 35px;">
                            <li class=""><i class="icofont icofont-simple-left"></i></li>
                            <li><a href="{{route('add-customer')}}" title="Tambah Pelanggan"> <i class="icofont icofont-plus"> </a></i></li>
                            <li><a href="{{ route('invitaion-form-customer') }}" ><i class="ti-email minimize-card" title="Kirim Undangan"></i> </a></li>
                            <li><i class="icofont icofont-refresh"></i></li>
                            <li><i class="icofont icofont-error "></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block">
                    <table class="table table-hover text-nowrap data-table">
                        <thead>
                            <tr>
                                <th style="width: 10px"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telpn</th>
                                <th>Alamat</th>
                                <th>Status Undangan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $customer)
                            <tr>
                                <td></td>
                                <td>{{$customer->name}}</td>
                                <td>{{$customer->users->email}}</td>
                                <td>{{$customer->tlpn}}</td>
                                <td>{{$customer->address}}</td>
                                <td>{!! $customer->invitation_status() !!}</td>
                                <td>
                                    <div class="dropdown-primary dropdown open">
                                        <button class="btn btn-primary dropdown-toggle waves-effect waves-light " type="button" id="dropdown-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Aksi</button>
                                        <div class="dropdown-menu" aria-labelledby="dropdown-2" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                            <a class="dropdown-item waves-light waves-effect" href="#">Detail</a>
                                            <a class="dropdown-item waves-light waves-effect" href="{{ route('edit-customer', ['id_cusomter'=>$customer->id]) }}">Edit</a>
                                            <a class="dropdown-item waves-light waves-effect btn-delete-customer" href="{{ route('delete-customer', ['id_cusomter'=>$customer->id]) }}">Hapus</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item waves-light waves-effect" href="#">{!! $customer->invitation_status() !!}</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>

    $(document).on('click', '.btn-delete-customer', function(e){
        e.preventDefault();
        let url = $(this).attr('href');
        let type_reset = $(this).data('type-reset')
        let message ='Apakah Anda Yakin Ingi Menghapus Data ';
        sweet_alert("question", "Hapus Data", message, true).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url;
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                console.log("123")
            }
        })
    })
</script>
@endsection