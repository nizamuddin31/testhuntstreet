@extends('layout.layout-main')
@section('content')

<!-- Page-header start -->
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont icofont icofont-file-document bg-c-pink"></i>
                <div class="d-inline">
                    <h4>Sample Page</h4>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index.html">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Pages</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Sample page</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5> {{ $customer ? 'Edit Pelanggan' : 'Tambah Pelanggan' }}</h5>

                </div>
                <div class="card-block">
                    <form method="POST" action="{{ $customer ? route('edit-data-customer', ['id_cusomter' => $customer->id]) : route('add-data-customer') }}" enctype="multipart/form-data" id="form">
                    @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-form-label">Nama Pelanggan</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                        class="form-control form-control-round"
                                        placeholder="Nama Pelanggan"
                                        name="name" value="{{ $customer ? $customer->name : '' }}">
                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-form-label">Email Pelanggan</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                        class="form-control form-control-round"
                                        placeholder="Email Pelanggan"
                                        name="email" value="{{ $customer ? $customer->users->email : '' }}">
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-form-label">No tlpn Pelanggan</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                        class="form-control form-control-round"
                                        placeholder="No tlpn Pelanggan"
                                        name="tlpn" value="{{ $customer ? $customer->tlpn : '' }}">
                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                        class="form-control form-control-round"
                                        placeholder="Alamat Pelanggan"
                                        name="address" value="{{ $customer ? $customer->address : '' }}">
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <button type = "submit" class="btn btn-primary btn-round">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection