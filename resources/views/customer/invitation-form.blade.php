@extends('layout.layout-main')
@section('style-css')
<style>
.switch-all-customer {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch-all-customer input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@endsection
@section('content')
<!-- Page-header start -->
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont icofont icofont-file-document bg-c-pink"></i>
                <div class="d-inline">
                    <h4>Sample Page</h4>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index.html">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Pages</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Sample page</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->

<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Kirim Link Undangan</h5>
                </div>
                <div class="card-block">
                    <form method="POST" action="{{route('sent-invitation-link-customer')}}" enctype="multipart/form-data" id="form">
                        @csrf  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-12 col-form-label">Pilih Pelanggan</label>
                                    <div class="col-sm-12">
                                        <select name="id_customer[]" class= "form-control select2" id="sekect-customer" multiple disabled>
                                            <option></option>
                                            @foreach($customers as $customer)
                                                <option value="{{ $customer->id }}"> {{ $customer->name }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-sm-12 col-form-label">Seluruh Pelanggan</label>
                                <div class="col-sm-12">
                                    <label class="switch-all-customer">
                                        <input type="checkbox" checked id="switch-all-customer" name="is_all_customer">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type = "submit" class="btn btn-primary btn-round">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
    $('#switch-all-customer').change(function(){
        if ($('#switch-all-customer').prop('checked')) {
            $('#sekect-customer').prop('disabled', true);
        } else {
            $('#sekect-customer').prop('disabled', false);
        }
    })
</script>
@endsection